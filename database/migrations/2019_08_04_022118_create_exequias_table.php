<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExequiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exequias', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->date('fecha');
            $table->string('hora');
            $table->string('nombre',128);
            $table->integer('edad');
            $table->string('muerte');
            $table->string('responsable',128);
            $table->string('telefono',20);

            $table->timestamps();

            $table->unique(['fecha', 'hora'],'fecha_hora_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exequias');
    }
}
