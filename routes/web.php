<?php
Route::get('/', function () {
    return view('welcome');
});

Route::get('login', 'Auth\LoginController@showLoginForm');
//Route::get('/', 'Auth\LoginController@showLoginForm')->middleware('guest');


Route::get('dashboard','DashboardController@index')->name('dashboard');

Auth::routes();

Route::post('login','Auth\LoginController@login')->name('login');
Route::post('logout','Auth\LoginController@logout')->name('logout');


//Route::get('/home', 'HomeController@index')->name('home');

Route::resource('exequias', 'ExequiasController');