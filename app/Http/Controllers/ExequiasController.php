<?php

namespace App\Http\Controllers;

use App\Exequias;
use Illuminate\Http\Request;

class ExequiasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datos['exequias'] = Exequias::paginate(5);
        return view('exequias.index',$datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('exequias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $datosExequia = $request->except('_token');

        Exequias::insert($datosExequia);

        //return response()->json($datosExequia);
        return redirect('exequias')->with('Mensaje','Reserva agregado con exito.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Exequias  $exequias
     * @return \Illuminate\Http\Response
     */
    public function show(Exequias $exequias)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Exequias  $exequias
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $exequia = Exequias::findOrFail($id);

        return view('exequias.edit',compact('exequia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Exequias  $exequias
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $datosExequia = $request->except(['_token','_method']);

        Exequias::where('id','=',$id)->update($datosExequia);

        //$exequia = Exequias::findOrFail($id);
        //return view('exequias.edit', compact('exequia'));
        return redirect('exequias')->with('Mensaje','Reserva modificada con éxito.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Exequias  $exequias
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Exequias::destroy($id);

        return redirect('exequias')->with('Mensaje','Reserva eliminada con éxito.');
    }
}
