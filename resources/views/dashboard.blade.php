@extends('layouts.app')

@section('content')

<!--
  <div class="d-flex justify-content-center h-100">
  	<div class="card dashboard">
  		<div class="card-header">
  			<h3>Bienvenido {{ auth()->user()->name }}</h3>
      </div>
      <div class="card-body">
        <strong>Email:</strong> {{ auth()->user()->email }}
      </div>
      <div class="card-footer">
        <form class="form" action="{{ route('logout') }}" method="post">
          @csrf
          <button class="btn btn-danger btn-xs btn-block">Cerrar Sesión</button>
        </form>
      </div>
    </div>
  </div>
-->

  <form-component></form-component>

    <reservation-component></reservation-component>

  </div>


@endsection
