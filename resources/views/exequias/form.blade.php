{{ $modo='crear' ? 'Agregar':'Modificar' }}

<div class="form-group">
    <label for="fecha" class="control-label">{{ 'Fecha' }}</label>
    <input type="date" class="form-control" name="fecha" id="fecha" value="{{ isset($exequia->fecha)?$exequia->fecha:'' }}"><br/>
</div>
<div class="form-group">
    <label for="hora" class="control-label">{{ 'Hora' }}</label>
    <select name="hora" class="form-control" id="hora">
        <option value="8" @if(isset($exequia->hora))
            @if ($exequia->hora=="8") selected @endif
        @endif>8:00 a.m.</option>
        <option value="9" @if(isset($exequia->hora))
            @if ($exequia->hora=="9") selected @endif
        @endif>9:00 a.m.</option>
        <option value="10" @if(isset($exequia->hora))
            @if ($exequia->hora=="10") selected @endif
        @endif>10:00 a.m.</option>
        <option value="11" @if(isset($exequia->hora))
            @if ($exequia->hora=="11") selected @endif
        @endif>11:00 a.m.</option>
        <option value="14" @if(isset($exequia->hora))
            @if ($exequia->hora=="14") selected @endif
        @endif>2:00 p.m.</option>
        <option value="15" @if(isset($exequia->hora))
            @if ($exequia->hora=="15") selected @endif
        @endif>3:00 p.m.</option>
        <option value="16" @if(isset($exequia->hora))
            @if ($exequia->hora=="16") selected @endif
        @endif>4:00 p.m.</option>
        <option value="17" @if(isset($exequia->hora))
            @if ($exequia->hora=="17") selected @endif
        @endif>5:00 p.m.</option>
        <option value="18" @if(isset($exequia->hora))
            @if ($exequia->hora=="18") selected @endif
        @endif>6:00 p.m.</option>
    </select>
</div>
<div class="form-group">
    <label for="nombre" class="control-label">{{ 'Nombre' }}</label>
    <input type="text" class="form-control" name="nombre" id="nombre" value="{{ isset($exequia->nombre)?$exequia->nombre:'' }}">
</div>
<div class="form-group">
    <label for="edad" class="control-label">{{ 'Edad' }}</label>
    <input type="number" class="form-control" name="edad" id="edad" value="{{ isset($exequia->edad)?$exequia->edad:'' }}">
</div>
<div class="form-group">
    <label for="muerte" class="control-label">{{ 'Muerte' }}</label>
    <input type="text" class="form-control" name="muerte" id="muerte" value="{{ isset($exequia->muerte)?$exequia->muerte:'' }}">
</div>
<div class="form-group">
    <label for="responsable" class="control-label">{{ 'Responsable' }}</label>
    <input type="text" class="form-control" name="responsable" id="responsable" value="{{ isset($exequia->responsable)?$exequia->responsable:'' }}">
</div>
<div class="form-group">
    <label for="telefono" class="control-label">{{ 'Teléfono' }}</label>
    <input type="text" class="form-control" name="telefono" id="telefono" value="{{ isset($exequia->telefono)?$exequia->telefono:'' }}"><br/>
</div>

<input type="submit" class="btn btn-success" value="{{ $modo=='crear' ? 'Reservar':'Modificar' }}">
<a href="{{url('exequias')}}" class="btn btn-primary" >Regresar</a>
