@extends('layouts.app')

@section('content')
    <div class="container">
            
        <form action="{{ url('/exequias/' . $exequia->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            {{ method_field('PATCH') }}
            @include('exequias.form',['modo'=>'editar'])
        </form>
    
    </div>
@endsection