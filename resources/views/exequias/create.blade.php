@extends('layouts.app')

@section('content')
    <div class="container">

        <form action="{{ url('/exequias') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include('exequias.form',['modo'=>'crear'])    
        </form>

    </div>
@endsection