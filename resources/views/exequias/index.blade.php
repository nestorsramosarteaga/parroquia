@extends('layouts.app')

@section('content')

    <div class="container">
        @if(Session::has('Mensaje'))
        {{ Session::get('Mensaje') }}
        @endif

        <a href="{{url('exequias/create')}}" class="btn btn-success">Agregar Reserva <i class="fa fa-plus-square"></i></a><br/><br/>

        <table class="table table-light table-hover">
            <thead class="thead-light">
                <tr>
                    <th>#</th>
                    <th>Fecha</th>
                    <th>Hora</th>
                    <th>Nombre</th>
                    <th>Edad</th>
                    <th>Muerte</th>
                    <th>Responsable</th>
                    <th>Telefóno</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($exequias as $exequia)
                    <tr>
                        <td>{{ $loop->iteration}}</td>
                        <td>{{ $exequia->fecha}}</td>
                        <td>{{ $exequia->hora}}</td>
                        <td>{{ $exequia->nombre}}</td>
                        <td>{{ $exequia->edad}}</td>
                        <td>{{ $exequia->muerte}}</td>
                        <td>{{ $exequia->responsable}}</td>
                        <td>{{ $exequia->telefono}}</td>
                        <td>
                            <a href="{{ url('/exequias/'.$exequia->id.'/edit') }}" class="btn btn-warning btn-sm" title="Editar"> <i class="fa fa-edit"></i></a>                            
                            <form action="{{ url('/exequias/'.$exequia->id) }}" method="post" class="btn-inline">
                                @csrf
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('¿Desea borrar el registro?');" title="Borrar"> <i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>            
                @endforeach
            </tbody>
        </table>
    </div>
@endsection