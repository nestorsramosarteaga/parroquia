@extends('layouts.app')

@section('content')
<div class="d-flex justify-content-center h-100">
	<div class="card">
		<div class="card-header">
			<h3>Sistema de Reservas</h3>
			<div class="d-flex justify-content-end social_icon">
				<!--
				<span><i class="fab fa-facebook-square"></i></span>

				<span><i class="fab fa-google-plus-square"></i></span>
				<span><i class="fab fa-twitter-square"></i></span>
				-->
			</div>
		</div>
		<div class="card-body">
			<form class="form" action="{{ route('login') }}" method="post">
				@csrf
				<div class="input-group form-group">
					<div class="input-group-prepend">
						<span class="input-group-text"><i class="fas fa-user"></i></span>
					</div>
					<input class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}"
						type="text"
						name="username"
						value="{{ old('username') }}"
						placeholder="Ingresa tu usuario">
						{!! $errors->first('username','<span class="form-text invalid-feedback">:message</span>') !!}

				</div>
				<div class="input-group form-group">
					<div class="input-group-prepend">
						<span class="input-group-text"><i class="fas fa-key"></i></span>
					</div>
					<input class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}"
						type="password"
						name="password"
						placeholder="Ingresa tu contraseña">
						{!! $errors->first('password','<span class="invalid-feedback">:message</span>') !!}
				</div>
				<!--
				<div class="row align-items-center remember">
					<input type="checkbox">Remember Me
				</div>
				-->
				<div class="form-group">
					<!--<input type="submit" value="Login" class="btn float-right login_btn">-->
					<button class="btn float-right login_btn">Acceder</button>
				</div>
			</form>
		</div>
		<div class="card-footer">
			<div class="d-flex justify-content-center links">
				¿No tiene una cuenta?<a href="{{ route('register') }}">Registrese!</a>
			</div>
			<!--
			<div class="d-flex justify-content-center">
				<a href="#">Forgot your password?</a>
			</div>
			-->
		</div>
	</div>
</div>
@endsection
