<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <title>Parroquia Jesus Nazareno</title>
    <meta charset="utf-8">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  </head>
  <body>
    <div class="container">
      <hr>
      @if(session()->has('flash'))
        <div class="alert alert-info">{{ session('flash') }}</div>
      @endif
      @yield('content')
    </div>
  </body>
</html>
