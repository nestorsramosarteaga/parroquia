require('./bootstrap');

window.Vue = require('vue');

Vue.component('form-component', require('./components/FormComponent.vue').default);
Vue.component('reservation-component', require('./components/ReservationComponent.vue').default);

const app = new Vue({
    el: '#app',
});
